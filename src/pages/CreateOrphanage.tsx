import React, { useState, createRef, FormEvent, ChangeEvent, useEffect } from "react";
import { Map, Marker, TileLayer } from 'react-leaflet';
import { LeafletMouseEvent } from 'leaflet'

import { FiPlus, FiXCircle } from "react-icons/fi";
import Sidebar from '../components/Sidebar'
import happyMapIcon from '../utils/mapIcon'

import '../styles/pages/create-orphanage.css';
import api from "../services/api";
import { useHistory } from "react-router";

export default function CreateOrphanage() {

  const history = useHistory()

  const [position, setPosition] = useState({latitude: 0, longitude: 0})
  const [openOnWeekends, setOpenOnWeekends] = useState(true)
  const [images, setImages] = useState<File[]>([])
  const [previewImages, setPreviewImages] = useState<string[]>([])

  const [myLocation, setMyLocation] = useState<[number, number]>([-27.2092052,-49.6401092])

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    }
  }
  
  function showPosition(position: any) {
    const { latitude, longitude } = position.coords
    setMyLocation([latitude, longitude])
  }

  useEffect(() => getLocation(), [])

  const form = {
    name: createRef<HTMLInputElement>(),
    about: createRef<HTMLTextAreaElement>(),
    instructions: createRef<HTMLTextAreaElement>(),
    opening_hours: createRef<HTMLInputElement>(),
  } as any

  const handleMapClick = (e: LeafletMouseEvent) => {
    const { lat, lng } = e.latlng
    
    setPosition({
      latitude: lat,
      longitude: lng
    })
  }

  const handleSelectImages = (e: ChangeEvent<HTMLInputElement>) => {
    if(!e.target.files) return

    const selectedImages = Array.from(e.target.files)
    setImages(selectedImages)

    const selectedImagesPreview = selectedImages.map(image => {
      return URL.createObjectURL(image)
    })

    setPreviewImages(selectedImagesPreview)
  }

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault()

    const formKeys = Object.keys(form)

    const result = [{}, ...formKeys]
    .reduce((object: any, key: any) => {
      object[key] = form[key].current.value
      return object
    })

    const unformatedData = {
      ...result,
      ...position,
      open_on_weekends: openOnWeekends,
      images,
    } as any

    const dataKeys = Object.keys(unformatedData)
    const data = new FormData()

    const appendOnData = (key: string, value: any) => data.append(key, value)

    dataKeys.forEach(key => {
      const value = unformatedData[key]

      if(typeof value === "object") {
        value.forEach((item: any) => appendOnData(key, item))
      }
      else appendOnData(key, String(value))
    })

    await api.post('orphanages', data)

    alert('Cadastro finalizado com sucesso!')

    history.push('/app')

  }

  const removeImage = (index: number) => {

    setPreviewImages(previewImages.filter((img, i) => index !== i))
    setImages(images.filter((img, i) => index !== i))
  }

  return (
    <div id="page-create-orphanage">

      <Sidebar />

      <main>
        <form className="create-orphanage-form" onSubmit={handleSubmit}>
          <fieldset>
            <legend>Dados</legend>

            <Map 
              center={myLocation} 
              style={{ width: '100%', height: 280 }}
              zoom={15}
              onClick={handleMapClick}
            >
              <TileLayer 
                url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/256/{z}/{x}/{y}@2x?access_token=${process.env.REACT_APP_MAPBOX_TOKEN}`}
              />

              { position.latitude !== 0 &&
                <Marker 
                  interactive={false} 
                  icon={happyMapIcon} 
                  position={[position.latitude,position.longitude]} 
                />
              }
            </Map>

            <div className="input-block">
              <label htmlFor="name">Nome</label>
              <input id="name" ref={form.name}/>
            </div>

            <div className="input-block">
              <label htmlFor="about">Sobre <span>Máximo de 300 caracteres</span></label>
              <textarea id="name" maxLength={300} ref={form.about} />
            </div>

            <div className="input-block">
              <label htmlFor="images">Fotos</label>

              <div className="images-container">

                {
                  previewImages.map((image, i) => {
                    return (
                      <div key={image} className="preview-image">
                        <FiXCircle className="remove-image" onClick={()=> removeImage(i)} />
                        <img src={image} alt={images[i].name} title={images[i].name} />
                      </div>
                    )
                  })
                }

                <label htmlFor="image[]" className="new-image">
                  <FiPlus size={24} color="#15b6d6" />
                </label>
              </div>

              <input multiple type="file" id="image[]" onChange={handleSelectImages} />

            </div>
          </fieldset>

          <fieldset>
            <legend>Visitação</legend>

            <div className="input-block">
              <label htmlFor="instructions">Instruções</label>
              <textarea id="instructions"  ref={form.instructions} />
            </div>

            <div className="input-block">
              <label htmlFor="opening_hours">Horários</label>
              <input id="opening_hours" ref={form.opening_hours} />
            </div>

            <div className="input-block">
              <label htmlFor="open_on_weekends">Atende fim de semana</label>

              <div className="button-select">
                <button type="button" 
                  className={openOnWeekends ? "active" : ""} 
                  onClick={() => setOpenOnWeekends(true)}>Sim</button>
                
                <button type="button"
                  className={!openOnWeekends ? "active" : ""} 
                  onClick={() => setOpenOnWeekends(false)}>Não</button>
              </div>
            </div>
          </fieldset>

          <button className="confirm-button" type="submit">
            Confirmar
          </button>
        </form>
      </main>
    </div>
  );
}

// return `https://a.tile.openstreetmap.org/${z}/${x}/${y}.png`;
