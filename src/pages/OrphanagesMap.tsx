import React, { useEffect, useState } from 'react';

import { Link } from 'react-router-dom'
import { FiPlus, FiArrowRight } from 'react-icons/fi'

// https://react-leaflet.js.org/docs/en/intro
// https://www.mapbox.com/
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import Leaflet from 'leaflet'

import markerImg from '../images/map-marker.svg'

import '../styles/pages/orphanages-map.css'

import api from '../services/api'

const mapIcon = Leaflet.icon({
  iconUrl: markerImg,

  iconSize: [58, 68],
  iconAnchor: [29, 68],
  popupAnchor: [178, 2],
})

interface Orphanage {
  id: number;
  name: string;
  latitude: number;
  longitude: number;
}

function OrphanagesMap() {

  const [orphanages, setOrphanages] = useState<Orphanage[]>([])

  useEffect(() => {
    api.get('/orphanages')
    .then(response => {
      setOrphanages(response.data)
    })
  }, [])

  return (
    <div id="page-map" className="App">
      
      <aside>
        <header>
          <img src={markerImg} alt="Happy" />

          <h2>Escolha um orfanato no mapa...</h2>
          <p>Muitas crianças estão esperando sua visita :)</p>
        </header>

        <footer>
          <strong>São Paulo</strong>
          <span>SP</span>
        </footer>
      </aside>

      <Map 
        center={[-23.598900, -46.741441]}
        zoom={15}
        style={{
          width: '100%',
          height: '100%'
        }}
      >
        {/* <TileLayer url="https:a.tile.openstreetmap.org/{z}/{x}/{y}.png" /> */}
        <TileLayer 
          url={`https://api.mapbox.com/styles/v1/mapbox/light-v10/tiles/256/{z}/{x}/{y}@2x?access_token=${process.env.REACT_APP_MAPBOX_TOKEN}`}
        />
        {/* https://openmaptiles.org/styles/ */}
      
        {
          orphanages.map(o => {
            return (
              <Marker 
                position={[o.latitude, o.longitude]}
                icon={mapIcon}
                key={o.id}
              >
                <Popup 
                  closeButton={false}
                  minWidth={240}
                  maxWidth={240}
                  className="map-popup"
                >
                  {o.name}
      
                  <Link to={`/orphanage/${o.id}`}>
                    <FiArrowRight size={20} color="#FFF" />
                  </Link>
                </Popup>
              </Marker>
            )
          })
        }
      </Map>

      <Link to="/orphanage/create" className="create-orphanage">
        <FiPlus size={32} color="#FFF" />
      </Link>

    </div>
  );
}

export default OrphanagesMap;
